const csv=require("csvtojson");
const ipl = require('./ipl.js');
var http=require('http');
const test=require('./testserver');
var fs=require('fs');
const extra=require('./Extraquesstion.js');

csv({
    checkType:true
}).fromFile('../data/matches.csv').then(matchdata=>{
    ipl.numberofmatchperyear(matchdata);
    ipl.MatchWonPerTeam(matchdata);
    extra.EachTeamWontossandWonMatch(matchdata);
    extra.PlayerPerSeasonWonAward(matchdata);

    csv({
        checkType:true
    }).fromFile('../data/deliveries.csv').then(deliveriesdata=>{
        ipl.ExtraRunConcededPerTeam(matchdata,deliveriesdata,2016);
        ipl.Top10EconomicalBowler(matchdata,deliveriesdata,2015);
        extra.viratstrikerate(matchdata,deliveriesdata);
        extra.HighestNumbeOfPlayerDismisaal(deliveriesdata);
        extra.TopEconomicalBowlerInSuperOver(deliveriesdata);
    });

});

    

var server=http.createServer(function(request,response){
    let url=request.url;
    console.log(url);
    url=url.toLowerCase();
    switch(url)
    {
        case "/top10economicalbowlerdata":
            {
                fs.readFile('../output/Top10EconomicalBowler.json',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/json'})
                        response.write("File does not exist. Please try again later")
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type': 'application/JSON'})
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }
            case "/top10economicalbowler":
                {
                    fs.readFile('./top10economical.html',function(err,data){
                        if(err)
                        {
                            response.writeHead(404,{'Content-Type':'text/html'})
                            response.write("File does not exist. Please try again later")
                            response.end();
                        }
                        else
                        {
                            
                            response.writeHead(200,{'Content-Type': 'text/html'})
                            response.write(data);
                            response.end();
                        }
                    });
                    break;
                }    
        
        case "/matchesperyeardata" :
            {
                fs.readFile('../output/matchesPerYear.json',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/html'})
                        response.write("File does not exist. Please try again later")
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type': 'text/json'})
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }
        case "/matchesperyear" :
            {
                fs.readFile('./matchesperyeargraph.html',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/html'})
                        response.write("File does not exist. Please try again later")
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type': 'text/html'})
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }
        case "/eachteamwontossandwonmatchdata" :
            {
                fs.readFile('../output/EachTeamWontossandWonMatch.json',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/json'})
                        response.write("File does not exist. Please try again later")
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type':'text/html'})
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }
            case "/eachteamwontossandwonmatch" :
                {
                    fs.readFile('./wontosswonmatch.html',function(err,data){
                        if(err)
                        {
                            response.writeHead(404,{'Content-Type':'text/html'})
                            response.write("File does not exist. Please try again later")
                            response.end();
                        }
                        else
                        {
                            response.writeHead(200,{'Content-Type':'text/html'})
                            response.write(data);
                            response.end();
                        }
                    });
                    break;
                }
        case "/extrarunconcededperteamdata" :
            {
                fs.readFile('../output/ExtraRunConcededPerTeam.json',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/json'})
                        response.write("File does not exist. Please try again later")
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type':'text/json'})
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }
        case "/extrarunconcededperteam" :
            {
                fs.readFile('./extrarunconceed.html',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/html'})
                        response.write("File does not exist. Please try again later")
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type':'text/html'})
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }
        case "/matchwonperteamdata" :
            {
                fs.readFile('../output/MatchWonPerTeam.json',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/json'})
                        response.write("File does not exist. Please try again later")
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type':'text/json'})
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }
        case "/matchwonperteam" :
            {
                fs.readFile('./matcheswonperteamgraph.html',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/html'})
                        response.write("File does not exist. Please try again later ???")
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type':'text/html'})
                        response.write(data);
                        response.end();
                    }
                });
                break ;
            }
                 
        case "/highestnumbeofplayerdismisaaldata" :
            {
                fs.readFile('../output/HighestNumbeOfPlayerDismisaal.json',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/json'})
                        response.write("File does not exist. Please try again later")
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type':'text/json'})
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }
        case "/highestnumbeofplayerdismisaal" :
            {
                fs.readFile('./highestdismissal.html',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/html'})
                        response.write("File does not exist. Please try again later")
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type':'text/html'})
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }
        case "/playerperseasonwonawarddata" :
            {
                fs.readFile('../output/PlayerPerSeasonWonAward.json',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/json'})
                        response.write("File does not exist. Please try again later");
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type':'text/html'})
                        response.write(data);
                        response.end();
                    }
                });
                break;
            } 
            case "/playerperseasonwonaward" :
            {
                fs.readFile('./playerwonaward.html',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/html'})
                        response.write("File does not exist. Please try again later");
                        response.end();
                    }
                    else
                    {
                        response.writeHead(404,{'Content-Type':'text/html'})
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }       
        case "/viratstrikeratedata" :
            {
                fs.readFile('../output/viratstrikerate.json',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/json'})
                        response.write("File does not exist. Please try again later");
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type':'text/json'})
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }
            case "/viratstrikerate" :
                {
                    fs.readFile('./viratgraph.html',function(err,data){
                        if(err)
                        {
                            response.writeHead(404,{'Content-Type':'text/html'})
                            response.write("File does not exist. Please try again later");
                            response.end();
                        }
                        else
                        {
                            response.writeHead(200,{'Content-Type':'text/html'})
                            response.write(data);
                            response.end();
                        }
                    });
                    break;
                }
        case "/topeconomicalbowlerinsuperoverdata" :
            {
                fs.readFile('../output/TopEconomicalBowlerInSuperOver.json',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/json'})
                        response.write("File does not exist. Please try again later");
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type':'text/json'})
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }
            case "/topeconomicalbowlerinsuperover" :
                {
                    fs.readFile('./superovereconomy.html',function(err,data){
                        if(err)
                        {
                            response.writeHead(404,{'Content-Type':'text/html'})
                            response.write("File does not exist. Please try again later");
                            response.end();
                        }
                        else
                        {
                            response.writeHead(200,{'Content-Type':'text/html'})
                            response.write(data);
                            response.end();
                        }
                    });
                    break;
                }
    
        case "/" :
            {
                fs.readFile('./htmlfile.html',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/html'})
                        response.write("File does not exist. Please try again later");
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type':'text/html'});
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }    
        default :
        {
            response.writeHead(404,{
                'Content-Type':'text/html'
            });
            response.write("Opps url is not present!");
            response.end();
            break;
        }
    }
});
server.listen(5000);
