const fs= require("fs");


/* Reteriving the number of matches played per year
 Example Output:
     {
        "2009": 57,
        "2010": 60,
        "2011": 73
      }
 */

module.exports.numberofmatchperyear= function numberofmatchperyear(matchdata){
    var numberofmatchperyear=matchdata.reduce((acc, cur) => {
        if(acc[cur.season]){
            acc[cur.season]++;
        }else{
            acc[cur.season] = 1;
        }
        return acc;
    }, {})
    fs.writeFile("../output/matchesPerYear.json",JSON.stringify(numberofmatchperyear),function(err){
        if(err) throw err;
    });
}

/* Getting the number of matches won per team per year
 Example Output:
[
    {
        "2008": {
            "Kolkata Knight Riders": 6,
            "Chennai Super Kings": 9,
            "Delhi Daredevils": 7,
            "Royal Challengers Bangalore": 4
        }
    },
    {
        "2009": {
            "Mumbai Indians": 5,
            "Royal Challengers Bangalore": 9,
            "Delhi Daredevils": 10
        }
    }
]
 */


module.exports.MatchWonPerTeam=function MatchWonPerTeam(matchdata){
    var teamname={};
    teamname=matchdata.reduce((intial,current)=>{
        if(intial[current.winner])
        {
            if(intial[current.winner][current.season])
            {
                intial[current.winner][current.season]++;
            }
            else
                intial[current.winner][current.season]=1;
        }
        else
        {
            if(current.winner !="")
            {
                intial[current.winner]={};
                intial[current.winner][current.season]=1;
            }
        }
        return intial;
        },{})
        let a=Object.keys(teamname);
        
        let teamnamearr=[];
        for(var i in teamname)
        {
            let data=[null,null,null,null,null,null,null,null,null,null];
            for(var j in teamname[i])
                {
                    //let index=parseInt(j)-2008;
                    let index=j-2008;
                    data[index]=teamname[i][j];
                }
            let obj={
                name:i,
                data:data
            }
            teamnamearr.push(obj)
        }
        fs.writeFile("../output/MatchWonPerTeam.json",JSON.stringify(teamnamearr),function(err){
            if(err) throw err;
        });
}

/* Getting extra run conceded per team in the given year
Example Output:
    [["Rising Pune Supergiants",108],["Mumbai Indians",102]]
]
*/
module.exports.ExtraRunConcededPerTeam=function ExtraRunConcededPerTeam(matchdata,deliveriesdata,year){

        let match_id=matchdata.reduce((intial,current)=>{
            if(current.season===year)
            {
                intial[current.id]=1;
            }
            return intial;
        },{});

        let result=deliveriesdata.reduce((intial,current)=>{
            if(match_id[current.match_id])
            {
               if( intial[current.bowling_team])
               {
                    intial[current.bowling_team]+=current.extra_runs;
               }
               else
               {
                   intial[current.bowling_team]=current.extra_runs;
               }
            }
            return intial;
        },{})
        //resutl hold the extra run conced per team data 
        result=Object.entries(result);
       
        fs.writeFile("../output/ExtraRunConcededPerTeam.json",JSON.stringify(result),function(err){
            if(err) throw err;
        });
}

/* Getting top economical bowlers in the given year
Example Output:
[["RN ten Doeschate",4],["J Yadav",4.142857142857142]]
*/

module.exports.Top10EconomicalBowler=function Top10EconomicalBowler(matchdata,deliveriesdata,year){
        var playername={};
        var legbyrun={};
        var byerun={};
        var widerun={};
        var noballrun={};
        var totalrun={};

        let match_id=matchdata.reduce((intial,current)=>{
            if(current.season===year)
            {
                intial[current.id]=1;
            }
            return intial 
        },{});
    
        let result=deliveriesdata.reduce((intial,current)=>{
            if(match_id[current.match_id])
            {
                var bowlername=current.bowler;
                if(playername[bowlername])
                {
                    totalrun[bowlername]+=(current.total_runs);
                    playername[bowlername]=playername[bowlername]+1;
                    legbyrun[bowlername]+=(current.legbye_runs);
                    byerun[bowlername]+=(current.bye_runs);
                    if((current.wide_runs)!=0)
                    {
                        widerun[bowlername]++;
                    }
                    if((current.noball_runs)!=0)
                    {
                        noballrun[bowlername]++;
                    }
                    intial[bowlername]=((totalrun[bowlername])-
                    (legbyrun[bowlername]+byerun[bowlername]))/
                    (playername[bowlername]-(widerun[bowlername]+noballrun[bowlername]))*6;
                            
                }
                else
                {
                    playername[bowlername]=1;
                    totalrun[bowlername]=(current.total_runs);
                    legbyrun[bowlername]=(current.legbye_runs);
                    byerun[bowlername]=(current.bye_runs);
                    widerun[bowlername]=(current.wide_runs);
                    noballrun[bowlername]=(current.noball_runs);
                    intial[bowlername]=((totalrun[bowlername])-(legbyrun[bowlername]+byerun[bowlername]))/
                    (playername[bowlername])*6;
                }
            }
            return intial;
            
        },{})

       // console.log(totalrun);
        
        var arr=[];
        arr=Object.entries(result);
       // console.log(arr);
        //sort the economical bowler 
        arr.sort(function(a,b){
            return a[1]-b[1];
        })
        //picking the top ten economical bowler
        var resultarr=arr.slice(0,10);
        fs.writeFile("../output/Top10EconomicalBowler.json",JSON.stringify(resultarr),function(err){
            if(err) throw err;
        });
}