const http=require('http');
const fs=require('fs');
const testserver=http.createServer((req,response)=>{
    let url=req.url;
    url=url.toLowerCase();
    switch(url)
    {
        case "/top10economicalbowlerdata":
            {
                fs.readFile('../output/Top10EconomicalBowler.json',function(err,data){
                    if(err)
                    {
                        response.writeHead(404,{'Content-Type':'text/json'})
                        response.write("File does not exist. Please try again later")
                        response.end();
                    }
                    else
                    {
                        response.writeHead(200,{'Content-Type': 'application/JSON'})
                        console.log(data)
                        response.write(data);
                        response.end();
                    }
                });
                break;
            }
        default :
        {
            response.writeHead(404,{
                'Content-Type':'text/html'
            });
            response.write("Opps url is not present! in testing");
            response.end();
            break;
        }
    }
})
testserver.listen(1111);
