const csvtojason=require("csvtojson");
const fs= require("fs");

/* Getting the number of times each team won the toss and won the match too

Example Output:
    [["Rising Pune Supergiant",5],["Kolkata Knight Riders",44]]

*/

module.exports.EachTeamWontossandWonMatch=function EachTeamWontossandWonMatch(matchdata){
let eachteamwon={};
    eachteamwon=matchdata.reduce((intial,curre)=>{
        let tosswinner=curre.toss_winner;
        let winner=curre.winner;
        if(tosswinner===winner)
        {
            if(intial[tosswinner])
            {
                intial[tosswinner]++;
            }
            else
            {
                intial[tosswinner]=1;
            }
        }
        return intial;
    },{})
    eachteamwon=Object.entries(eachteamwon);
    fs.writeFile("../output/EachTeamWontossandWonMatch.json",JSON.stringify(eachteamwon),(err)=>{
        if(err) throw err;
    });
}

/* Returning the player per season who has won the highest number of player of the match in that season

 Example Output:
     [{"name":"SE Marsh","data":[5]},{"name":"YK Pathan","data":[null,3]},{"name":"SR Tendulkar","data":[null,null,4]},]

*/

module.exports.PlayerPerSeasonWonAward=function PlayerPerSeasonWonAward(matchdata){ 
    var playername={};
    playername=matchdata.reduce((intial,curre)=>{
        let season=curre.season;
        let playerofmatch=curre.player_of_match;
        if(intial[season])
        {
            if(intial[season][playerofmatch])
            {
                intial[season][playerofmatch]++;
            }
            else
            {
                intial[season][playerofmatch]=1;
            }
        }
        else
        {
            intial[season]={};
            intial[season][playerofmatch]=1;
        }
        return intial;
    },{});
    let arr=[];
    
    for(let key in playername)
    {
        arr=Object.entries(playername[key]); 
        arr.sort((a,b)=> b[1]-a[1]);
        playername[key]=arr.slice(0,1);

    }
    var arr1 = [];
    for(let i in playername)
    {
        arr1.push(playername[i][0])
    }
   let resultarr=[];
   for(var i=0;i<arr1.length;i++)
   {
       let data=[];
       data[i]=arr1[i][1];
       let obj={
           name :arr1[i][0],
           data :data
       };
       resultarr.push(obj);

   }
   
    fs.writeFile('../output/PlayerPerSeasonWonAward.json',JSON.stringify(resultarr),(err)=>{
        if(err) throw err;
    });
}
/* Writing strike rate of virat kohli per season
    Example Output:
    {
        "2008": 105.1,
        "2009": 112.33,
        "2010": 144.81
    }
*/

module.exports.viratstrikerate= function viratstrikerate(matchdata,deliveriesdata){
        var totalrun={};
        var totalball={};
        var total_wide={};
        let matchid=matchdata.reduce((intial,curr)=>{
            var id=curr.id;
            if(intial[id]){
            intial[id]=curr.season;
            }
            else{
            intial[id]=curr.season;
            }
            return intial;
        },{});

        let virateconomy=deliveriesdata.reduce((intial,curr)=>{
            let batsmanneme=curr.batsman;
            if(batsmanneme=='V Kohli' && matchid[curr.match_id])
            {
                let season=matchid[curr.match_id];
                if(intial[season])
                {
                    totalball[season]++;
                    intial[season]++;
                    totalrun[season]+=(curr.batsman_runs);
                    if((curr.wide_runs)!=0)
                         total_wide[season]++;
                    intial[season]=((totalrun[season])/((totalball[season])-(total_wide[season])))*100;  
                }
                else{
                    intial[season]=1;
                    totalball[season]=1;
                    totalrun[season]=(curr.batsman_runs);
                    if((curr.wide_runs)!=0)
                        total_wide[season]=1;
                    else
                    total_wide[season]=0;
                }
            }
            return intial;
        },{})

        fs.writeFile('../output/viratstrikerate.json',JSON.stringify(virateconomy),function(err){
                    if(err) throw err;
                });
}
/* Getting the highest number of times a player is dismissed by another player in all season
 Example Output:
[["MS Dhoni By Z Khan",7]]
*/

module.exports.HighestNumbeOfPlayerDismisaal=function HighestNumbeOfPlayerDismisaal(deliveriesdata){
    
        let result=deliveriesdata.reduce((intial,current)=>{
            if(current.player_dismissed!=='' && current.dismissal_kind!=='run out')
            {
                if(intial[current.player_dismissed])
                {
                    if(intial[current.player_dismissed][current.bowler])
                        {
                            intial[current.player_dismissed][current.bowler]++;
                        }
                    else
                    intial[current.player_dismissed][current.bowler]=1;
                }
                else
                {
                    intial[current.player_dismissed]={};
                    intial[current.player_dismissed][current.bowler]=1;

                }
            }
            return intial;
        },{})

        //console.log(result)
        
        var max = 0 ;
        var arr = [];
      //  sort the result object to pic one player which got highest dismissal of other player
        for(let keys in result){
            for(let subkey in result[keys]){
                if(max < result[keys][subkey]){
                    arr.push([keys, subkey, result[keys][subkey]]);
                    max = result[keys][subkey];
                }
            }
        }
        
      //  result store the bottom one player has highest dismisal
        result = arr.pop();
        let ar=[];
        let arrr=[];
        arrr.push(result[0]+" By "+result[1],result[2]);
        ar.push(arrr);
        fs.writeFile('../output/HighestNumbeOfPlayerDismisaal.json',JSON.stringify(ar),(err)=>{
            if(err) throw err;
        });
}
/* Returning the bowlers economies in super overs

    Example Output:
    [["JJ Bumrah",4],["J Theron",10.8],["R Rampaul",11],["SP Narine",11],["JP Faulkner",11.454545454545455]]
*/

module.exports.TopEconomicalBowlerInSuperOver=function TopEconomicalBowlerInSuperOver(deliveriesdata){
        let bowlername={};
        var pnametotalrun={};
        var pname={};
        var pnameleg={};
        var pnameby={};
        var pnamewide={};
        var pnamenoball={};
        bowlername=deliveriesdata.reduce((intial,current)=>{
         var bowlername=current.bowler;
         if(pname[bowlername] && (current.is_super_over)!=0){
            pnametotalrun[bowlername]+=(current.total_runs);
            pname[bowlername]=(pname[bowlername])+1;
            pnameleg[bowlername]+=(current.legbye_runs);
            pnameby[bowlername]+=(current.bye_runs);
            if((current.wide_runs)!=0)
                pnamewide[bowlername]++
            
            if((current.noball_runs)!=0)
                pnamenoball[bowlername]++;
            intial[bowlername]=((pnametotalrun[bowlername])-(pnameleg[bowlername]+pnameby[bowlername]))
            /((pname[bowlername])-(pnamewide[bowlername]+pnamenoball[bowlername]))*6;
            
            }
        else
            {
             if(parseInt(current.is_super_over)!=0){
                pname[bowlername]=1;
                pnametotalrun[bowlername]=(current.total_runs);
                pnameleg[bowlername]=(current.legbye_runs);
                pnameby[bowlername]=(current.bye_runs);
                pnamewide[bowlername]=(current.wide_runs);
                pnamenoball[bowlername]=(current.noball_runs);
                intial[bowlername]=((pnametotalrun[bowlername])-(pnameleg[bowlername]+pnameby[bowlername]))
                /((pname[bowlername]))*6;
              } 
            }
             return intial;
        },{});
        
        let arr=[];
        arr=Object.entries(bowlername);
        //arr store the top five the economical bowler in super over
        arr=arr.sort((a,b)=>{
            return a[1]-b[1];
        }).slice(0,5);
        fs.writeFile('../output/TopEconomicalBowlerInSuperOver.json',JSON.stringify(arr),(err)=>{
            if(err) throw err;

        });
}